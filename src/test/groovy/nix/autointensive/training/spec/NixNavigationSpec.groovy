package nix.autointensive.training.spec

import geb.spock.GebReportingSpec
import nix.autointensive.training.page.StartPage
import nix.autointensive.training.page.BlogPage

class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to Blog page"(){
        when:
        to StartPage

        and:
        "User navigates to Blog page"()

        then:
        at BlogPage
    }
}
